package com.nure.ua.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

@Configuration
public class PropertyConfigurer extends PropertyPlaceholderConfigurer {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyConfigurer.class);
    private static final String CLASS_PATH ="application.properties";
    private static final String SPRING_PATH ="spring.properties";

    protected PropertyConfigurer() {

        String propertyHome = System.getProperty("APP_PROP");

        List<Resource> resources = new LinkedList<>();

        Resource resource;
        if (null == propertyHome) {
            LOGGER.info("Path to the external property file has not found. Default properties are going to load.");
            resource = new ClassPathResource(CLASS_PATH);
        } else {
            LOGGER.info("Path to the external property file found: " + propertyHome);
            Properties props = new Properties();
            try {
                props.load(new FileInputStream(propertyHome));
                LOGGER.info("External property file has been loaded");
                resource = new FileSystemResource(propertyHome);
            } catch (IOException e) {
                LOGGER.error("External property file has not found. Default properties are going to load.",e);
                resource = new ClassPathResource(CLASS_PATH);
            }
        }
        resources.add(resource);
        ClassPathResource springClassPath = new ClassPathResource(SPRING_PATH);
        resources.add(springClassPath);
        this.setLocations(resources.toArray(new Resource[resources.size()]));
    }

    @Bean
    public PropertyPlaceholderConfigurer getPropertyPlaceholderConfigurer() {

        PropertyConfigurer externalPropertyPlaceholder = new PropertyConfigurer();
        externalPropertyPlaceholder.setNullValue("null");
        externalPropertyPlaceholder.setIgnoreResourceNotFound(false);
        externalPropertyPlaceholder.setSearchSystemEnvironment(true);
        externalPropertyPlaceholder.setIgnoreUnresolvablePlaceholders(true);
        return externalPropertyPlaceholder;
    }
}
