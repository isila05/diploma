package com.nure.ua.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import java.util.Properties;

import static com.nure.ua.util.PropertiesReader.getProperties;

@Configuration
@Import(value = {
        DataSourceConfig.class,
        InfrastructureConfig.class,
        PropertyConfigurer.class,
        ServiceConfig.class,
        SwaggerConfig.class
})
@ComponentScan("com.nure.ua.controllers")
public class ApplicationConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfig.class);
    private static final Properties properties = getProperties("spring.properties");

    public static final String NAMING_FACTORY_KEY = properties.getProperty("naming.factory.key");
    public static final String NAMING_FACTORY_VALUE = properties.getProperty("naming.factory.value");

    @Autowired
    private Environment env;

    /**
     * Application custom initialization code.
     * <p/>
     * Spring profiles can be configured with a system property
     * -Dspring.profiles.active=javaee
     * <p/>
     */
    @PostConstruct
    public void initApp() {
        LOGGER.debug("Looking for Spring profiles...");
        if (env.getActiveProfiles().length == 0) {
            LOGGER.info("No Spring profile configured, running with default configuration.");
        } else {
            for (String profile : env.getActiveProfiles()) {
                LOGGER.info("Detected Spring profile: {}", profile);
            }
        }
    }

}
