package com.nure.ua.configuration;

import com.google.common.cache.CacheBuilder;
import com.nure.ua.model.shingles.MegaShinglesComparer;
import com.nure.ua.model.shingles.ShinglesComparer;
import com.nure.ua.model.shingles.SuperShinglesComparer;
import com.nure.ua.xls.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import static com.nure.ua.util.PropertiesReader.getProperties;


@Configuration
@EnableAsync
@EnableScheduling
@EnableAspectJAutoProxy
@EnableCaching
@EnableWebMvc
@ComponentScan(basePackages = {"com.nure.ua.configuration", "com.nure.ua.services"})
public class ServiceConfig extends WebMvcConfigurerAdapter implements AsyncConfigurer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceConfig.class);
    private static final Properties properties = getProperties("spring.properties");
    private static final String CACHE_NAME = properties.getProperty("app.cache.name");
    public static final Long CACHE_HOURS = Long.valueOf(properties.getProperty("app.cache.hours"));
    private int maxUploadSizeInMb = 5 * 1024 * 1024;

    @Bean
    public CacheManager cacheManager() {
        LOGGER.info("Configuring Spring CacheManager");

        SimpleCacheManager cacheManager = new SimpleCacheManager();
        GuavaCache cache = new GuavaCache(
                CACHE_NAME, CacheBuilder.newBuilder().expireAfterWrite(CACHE_HOURS, TimeUnit.HOURS).build()
        );
        cacheManager.setCaches(Collections.singletonList(cache));

        return cacheManager;
    }

    @Bean
    public Parser getParser(){
        return new Parser();
    }

    @Override
    public Executor getAsyncExecutor() {
        LOGGER.debug("Creating Async Task Executor");

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(64);
        executor.setThreadNamePrefix("MyExecutor-");
        executor.initialize();

        return executor;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return null;
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {

        CommonsMultipartResolver cmr = new CommonsMultipartResolver();
        cmr.setMaxUploadSize(maxUploadSizeInMb * 2);
        cmr.setMaxUploadSizePerFile(maxUploadSizeInMb);
        return cmr;

    }

    @Bean
    public ShinglesComparer getShinglesComparer(){
        return new ShinglesComparer(84);
    }

    @Bean
    public SuperShinglesComparer getSuperShinglesComparer(){
        return new SuperShinglesComparer();
    }

    @Bean
    public MegaShinglesComparer getMegaShinglesComparer(){
        return new MegaShinglesComparer();
    }
}