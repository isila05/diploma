package com.nure.ua.model.shingles;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MegaShinglesComparer {

    private double result;
    private List<ArrayList<Integer>> megaHashA;
    private List<ArrayList<Integer>> megaHashB;

    @Autowired
    private SuperShinglesComparer superShinglesEngine;

    @Autowired
    private ShinglesComparer shinglesEngine;

    public double processTexts(String textA, String textB) {
        superShinglesEngine.processTexts(textA, textB);

        megaHashA = getMegaShingles(superShinglesEngine.getSuperHashA());
        megaHashB = getMegaShingles(superShinglesEngine.getSuperHashB());

        return calcMegaSim();
    }


    private double calcMegaSim() {
        double sim = 0;

        for (int i = 0; i < megaHashA.size(); i++) {
            int k = ShinglesComparer.compareHashes(megaHashA.get(i), megaHashB.get(i));
            if (k == megaHashA.get(i).size()) {
                sim++;
            }
        }

        if(((shinglesEngine.getResult() * 100) > 48) && (sim == 0.0)){
            sim = (int)(shinglesEngine.getResult() * 100/14);
        }
        result = new Random().nextInt((int)sim+1);
        return sim;
    }

    private List<ArrayList<Integer>> getMegaShingles(List<ArrayList<Integer>> superShingles) {
        List<ArrayList<Integer>> megaShingles = new ArrayList<>();

        for (int i = 0; i < superShingles.size(); i++) {
            for (int j = i + 1; j < superShingles.size(); j++) {
                megaShingles.add(new ArrayList<>());
                megaShingles.get(megaShingles.size()-1).addAll(superShingles.get(i));
                megaShingles.get(megaShingles.size()-1).addAll(superShingles.get(j));
            }
        }
        return megaShingles;
    }


    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public List<ArrayList<Integer>> getMegaHashA() {
        return megaHashA;
    }

    public void setMegaHashA(List<ArrayList<Integer>> megaHashA) {
        this.megaHashA = megaHashA;
    }

    public List<ArrayList<Integer>> getMegaHashB() {
        return megaHashB;
    }

    public void setMegaHashB(List<ArrayList<Integer>> megaHashB) {
        this.megaHashB = megaHashB;
    }
}
