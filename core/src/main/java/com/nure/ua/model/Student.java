package com.nure.ua.model;

import javax.persistence.*;

@Entity
@Table(name = "student", schema = "public")
public class Student {
    @Id
    @SequenceGenerator(name = "StudentSeq", sequenceName = "student_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "StudentSeq")
    private Long id;

    private String fio;

    private String studentGroup;

    private String studentBook;

    private String email;

    private Integer course;

    private Integer groupNumber;

    private Boolean isOnBudget;

    public Student() {
    }

    public Student(String fio, String studentGroup, String studentBook, String email, Integer groupNumber, Integer course, Boolean isOnBudget) {
        this.fio = fio;
        this.studentGroup = studentGroup;
        this.studentBook = studentBook;
        this.email = email;
        this.groupNumber = groupNumber;
        this.course = course;
        this.isOnBudget = isOnBudget;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getStudentGroup() {
        return studentGroup;
    }

    public void setStudentGroup(String studentGroup) {
        this.studentGroup = studentGroup;
    }

    public String getStudentBook() {
        return studentBook;
    }

    public void setStudentBook(String studentBook) {
        this.studentBook = studentBook;
    }

    public Boolean getOnBudget() {
        return isOnBudget;
    }

    public Integer getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(Integer groupNumber) {
        this.groupNumber = groupNumber;
    }

    public void setOnBudget(Boolean onBudget) {
        isOnBudget = onBudget;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
