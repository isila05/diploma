package com.nure.ua.model.blockchain;

import com.nure.ua.bindings.BlockChainBinding;
import com.nure.ua.bindings.BlockChainDataBinding;
import com.nure.ua.services.StringUtil;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

@Entity
@Table(name = "blockchain", schema = "public")
public class Block {

    private static final org.slf4j.Logger LOGGER = getLogger(Block.class);

    @Id
    @SequenceGenerator(name = "BlockchainSeq", sequenceName = "blockchain_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BlockchainSeq")
    private Long id;

    private String hash;

    private String previousHash;

    private LocalDate date;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "blockchain_id")
    private List<BlockChainData> data;

    public Block(String previousHash) {
        this.previousHash = previousHash;
        this.date = LocalDate.now();
        this.hash = calculateHash();
    }

    public Block(String hash, String previousHash, LocalDate date, List<BlockChainData> data) {
        this.hash = hash;
        this.previousHash = previousHash;
        this.date = date;
        this.data = data;
    }

    public Block(BlockChainBinding binding) {
        this.date = binding.getDate() != null ? LocalDate.parse(binding.getDate()) : LocalDate.now();
        this.previousHash = binding.getPreviousHash();
        List<BlockChainData> list = new ArrayList<>();
        for (BlockChainDataBinding dataBinding : binding.getBlockchainData()) {
            list.add(new BlockChainData(dataBinding));
        }
        this.data = list;
        this.hash = calculateHash();
    }

    public String calculateHash() {
        StringBuilder stringBuilder = new StringBuilder();
        data.forEach(stringBuilder::append);
        return StringUtil.applySha256(previousHash + date.toEpochDay() + stringBuilder);
    }

    public Block() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<BlockChainData> getData() {
        return data;
    }

    public void setData(List<BlockChainData> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Block data{" +
                ", hash='" + hash + '\'' +
                ", previousHash='" + previousHash + '\'' +
                ", date=" + date +
                ", data=" + data +
                '}';
    }
}
