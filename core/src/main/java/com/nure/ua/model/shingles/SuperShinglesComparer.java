package com.nure.ua.model.shingles;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class SuperShinglesComparer {

    private double result;
    private List<ArrayList<Integer>> superHashA;
    private List<ArrayList<Integer>> superHashB;

    @Autowired
    private ShinglesComparer shinglesEngine;

    public double processTexts(String textA, String textB) {
        shinglesEngine.processTexts(textA, textB);

        superHashA = getSuperShingles(shinglesEngine.getFirstTextHashes(), 6);
        superHashB = getSuperShingles(shinglesEngine.getSecondTextHashes(), 6);

        return calcSuperSim();
    }

    private List<ArrayList<Integer>> getSuperShingles(List<Integer> hashes, int superCount) {
        List<ArrayList<Integer>> superShingles = new ArrayList<>();
        int count = hashes.size() / superCount;
        for (int i = 0; i < superCount; i++) {
            superShingles.add(new ArrayList<>());
            for (int j = 0; j < count; j++) {
                superShingles.get(i).add(hashes.get(i + superCount * j));
            }
        }
        return superShingles;
    }

    private double calcSuperSim() {
        double sim = 0;
        for (int i = 0; i < superHashA.size(); i++) {
            int k = ShinglesComparer.compareHashes(superHashA.get(i), superHashB.get(i));
            if (k == superHashA.get(i).size()) {
                sim++;
            }
        }
        if(((shinglesEngine.getResult() * 100) > 48) && sim == 0.0){
            sim = (int)(shinglesEngine.getResult() * 100/14);
        }
        result = sim;
        return result;
    }


    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public List<ArrayList<Integer>> getSuperHashA() {
        return superHashA;
    }

    public void setSuperHashA(List<ArrayList<Integer>> superHashA) {
        this.superHashA = superHashA;
    }

    public List<ArrayList<Integer>> getSuperHashB() {
        return superHashB;
    }

    public void setSuperHashB(List<ArrayList<Integer>> superHashB) {
        this.superHashB = superHashB;
    }
}
