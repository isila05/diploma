package com.nure.ua.model;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.IOException;

@Entity
@Table(name = "document", schema = "public")
public class Document {

    @Id
    @SequenceGenerator(name = "DocumentSeq", sequenceName = "document_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DocumentSeq")
    private Long id;

    //TODO
//    @ManyToOne(optional = true, fetch = FetchType.LAZY)
//    @JoinColumn(name = "id", nullable = true)

    private Long studentId;

    private String category;

    private String title;

    private String fileName;

    private String contentType;

    private String subject;

    private Boolean isPublic;

    public Document(Long studentId, String category, String title, String subject, MultipartFile multipartFile, Boolean isPublic) throws IOException {
        this.studentId = studentId;
        this.category = category;
        this.isPublic = isPublic;
        this.subject = subject;
        this.title = title;
        this.fileName = multipartFile.getOriginalFilename();
        this.contentType = multipartFile.getContentType();
    }

    public Document() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Boolean getPublic() {
        return isPublic;
    }

    public void setPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
