package com.nure.ua.model.blockchain;

import com.nure.ua.bindings.BlockChainDataBinding;

import javax.persistence.*;
import java.time.LocalDate;

import static org.slf4j.LoggerFactory.getLogger;

@Entity
@Table(name = "blockchain_data", schema = "public")
public class BlockChainData {

    @Id
    @SequenceGenerator(name = "BlockChainDataSeq", sequenceName = "blockchain_data_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BlockChainDataSeq")
    private Long id;

    private Long blockchain_id;

    private String title;

    private LocalDate creationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public Long getBlockchain_id() {
        return blockchain_id;
    }

    public void setBlockchain_id(Long blockchain_id) {
        this.blockchain_id = blockchain_id;
    }

    public BlockChainData() {
    }

    public BlockChainData(Long blockchain_id, String title, byte[] file, LocalDate creationDate) {
        this.blockchain_id = blockchain_id;
        this.title = title;
        this.creationDate = creationDate;
    }

    public BlockChainData(BlockChainDataBinding binding) {
        this.title = binding.getTitle();
        this.creationDate = LocalDate.parse(binding.getCreationDate());
    }

    @Override
    public String toString() {
        return "data{" +
                "title= '" + title + '\'' +
                ", creationDate= " + creationDate +
                '}';
    }
}
