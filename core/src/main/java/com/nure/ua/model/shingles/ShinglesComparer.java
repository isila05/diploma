package com.nure.ua.model.shingles;

import com.nure.ua.model.shingles.util.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.nure.ua.model.shingles.util.Constants.SHINGLE_LEN;
import static org.slf4j.LoggerFactory.getLogger;

public class ShinglesComparer {

    private double result;
    private List<Integer> firstTextHashes;
    private List<Integer> secondTextHashes;

    private int hashCount;

    private static final org.slf4j.Logger LOGGER = getLogger(ShinglesComparer.class);

    public ShinglesComparer(int hashCount) {
        this.hashCount = hashCount;
    }

    public double processTexts(String firstText, String secondText) {
        firstTextHashes = calcMinHashesFromText(firstText, hashCount);
        secondTextHashes = calcMinHashesFromText(secondText, hashCount);
        return estimateSimilarity(hashCount);
    }

    public static int compareHashes(List<Integer> hashes1, List<Integer> hashes2) {
        int sim = 0;
        for (int i = 0; i < hashes1.size(); i++) {
            if (hashes1.get(i).equals(hashes2.get(i))) {
                sim++;
            }
        }
        return sim;
    }

    private double estimateSimilarity(int hashCount) {
        double sim = compareHashes(firstTextHashes, secondTextHashes);
        sim /= (double) hashCount;
        result = sim;
        return sim;
    }

    private List<Integer> calcMinHashesFromText(String text, int hashCount) {
        return getMinHashes(calcHashes(getShingles(canonisation(text.toLowerCase())), hashCount));
    }

    private List<String> canonisation(String text) {
        for (String stopSymbol : Constants.STOP_SYMBOLS) {
            text = text.replace(stopSymbol, "");
        }

        for (String stopWord : Constants.STOP_WORDS_RU) {
            text = text.replace(" " + stopWord + " ", " ");
        }

        return Arrays.asList(text.split(" "));
    }

    private List<String> getShingles(List<String> words) {

        List<String> filteredWords = words.stream().filter(w -> w.length() > 0).collect(Collectors.toList());
        List<String> shingles = new ArrayList<>();

        int shinglesNumber = filteredWords.size() - SHINGLE_LEN;
        for (int i = 0; i <= shinglesNumber; i++) {
            StringBuilder shingle = new StringBuilder();

            //Create one shingle
            for (int j = 0; j < SHINGLE_LEN; j++) {
                shingle.append(filteredWords.get(i + j)).append(" ");
            }

            shingles.add(shingle.toString());
        }

        return shingles;
    }

    private List<ArrayList<Integer>> calcHashes(List<String> shingles, int countHashes) {
        List<ArrayList<Integer>> hashes = new ArrayList<>();
        for (int i = 0; i < countHashes; i++) {
            hashes.add(new ArrayList<>());

            for(String s: shingles){
                hashes.get(i).add(hashFunc(s, i));
            }
        }
        return hashes;
    }


    private List<Integer> getMinHashes(List<ArrayList<Integer>> hashes) {
        List<Integer> minHashes = new ArrayList<>();
        int hashNum = 0;

        for(ArrayList<Integer> hash: hashes){
            int min = hash.get(0);
            int k = 0;
            int shinMin = 0;

            for(Integer h: hash){
                if(min > h){
                    min = h;
                    shinMin = k;
                }
                k++;
            }
            LOGGER.info("Hash #" + hashNum + " Shingle - #" + shinMin);
            hashNum++;
            minHashes.add(min);
        }

        return minHashes;
    }

    private int hashFunc(String shingle, int num) {
        num += 84;
        return hash(shingle, 2, Constants.simpleNumbers[num]);
    }

    private int hash(String shingle, int p, int mod) {
        int hash = shingle.charAt(0);
        int m = 1;
        for (int i = 1; i < shingle.length(); i++, m*= p) {
            hash = (hash * p) % mod + shingle.charAt(i);
        }
        return hash % mod;
    }


    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public List<Integer> getFirstTextHashes() {
        return firstTextHashes;
    }

    public void setFirstTextHashes(List<Integer> firstTextHashes) {
        this.firstTextHashes = firstTextHashes;
    }

    public List<Integer> getSecondTextHashes() {
        return secondTextHashes;
    }

    public void setSecondTextHashes(List<Integer> secondTextHashes) {
        this.secondTextHashes = secondTextHashes;
    }

    public int getHashCount() {
        return hashCount;
    }

    public void setHashCount(int hashCount) {
        this.hashCount = hashCount;
    }
}
