package com.nure.ua.morpher;

import java.util.List;

public class NR {

    public static List<String> list(String name) {
        NameReflect nameReflect = new NameReflect(name);
        return nameReflect.getCaseList();
    }

    public static String kto(String name) {
        NameReflect n = new NameReflect(name);
        return n.getCaseList(NameReflect.UANAZYVNYI);
    }

    public static String kogo(String name) {
        NameReflect n = new NameReflect(name);
        return n.getCaseList(NameReflect.UARODOVYI);
    }

    public static String komy(String name) {
        NameReflect n = new NameReflect(name);
        return n.getCaseList(NameReflect.UADAVALNYI);
    }

    public static String kogoScho(String name) {
        NameReflect n = new NameReflect(name);
        return n.getCaseList(NameReflect.UAZNAHIDNYI);
    }

    public static String kem(String name) {
        return kim(name);
    }
    public static String kum(String name) {
        return kim(name);
    }

    public static String kim(String name) {
        NameReflect n = new NameReflect(name);
        return n.getCaseList(NameReflect.UAORUDNYI);
    }

    public static String naKomy(String name) {
        NameReflect n = new NameReflect(name);
        return n.getCaseList(NameReflect.UAMISZEVYI);
    }

    // кличний
    public static String kogoK(String name) {
        NameReflect n = new NameReflect(name);
        return n.getCaseList(NameReflect.UAKLYCHNYI);
    }
}