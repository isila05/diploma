package com.nure.ua.morpher;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;


@Deprecated
//TODO: check why it's working in console but throw an exception with spring model
public class Morpher {

    private Document doc;
    private String result;

    public Morpher(String word, String p) throws IOException, ParserConfigurationException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        this.doc = builder.parse("http://api.morpher.ru/WebService.asmx/GetXmlUkr?s=" + word.replace(" ", "%20"));
        if (null != this.doc) {
            Element root = this.doc.getDocumentElement();
            NodeList nodes = root.getChildNodes();
            for (int x = 0; x < nodes.getLength(); x++) {
                Node item = nodes.item(x);
                if (item instanceof Element) {
                    Element el = ((Element) item);
                    if (el.getTagName().equals(p)) {
                        result = ((Text) el.getFirstChild()).getData().trim();
                    }
                }
            }
        }
    }

    public Document getDoc() {
        return doc;
    }

    public void setDoc(Document doc) {
        this.doc = doc;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
