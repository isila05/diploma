package com.nure.ua.controllers;

import com.nure.ua.bindings.*;
import com.nure.ua.model.Document;
import com.nure.ua.model.Student;
import com.nure.ua.model.shingles.MegaShinglesComparer;
import com.nure.ua.model.shingles.ShinglesComparer;
import com.nure.ua.model.shingles.SuperShinglesComparer;
import com.nure.ua.services.DocumentService;
import com.nure.ua.services.StudentService;
import com.nure.ua.util.WordUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.nure.ua.util.LoggingResponse.*;
import static org.slf4j.LoggerFactory.getLogger;


@Api(value = "/document", description = "Operations with document")
@RestController
@RequestMapping("/document")
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private ShinglesComparer shComp;

    @Autowired
    private SuperShinglesComparer sshComp;

    @Autowired
    private MegaShinglesComparer mshComp;

    private static final org.slf4j.Logger LOGGER = getLogger(DocumentController.class);

    private static final Integer SUPERSTUDENT_ID = 1;

    @CrossOrigin
    @RequestMapping(value = "/uploadDocument/{studentId}", method = RequestMethod.POST, consumes = "multipart/form-data")
    public ResponseEntity uploadDocument(@PathVariable Long studentId, @RequestBody MultipartFile file,
                                       @RequestParam String title, @RequestParam String category,
                                       @RequestParam(required = false) String subject, @RequestParam Boolean isPublic) throws IOException {

        Student student = studentService.find(studentId);

        if (student == null) {
            return notFoundLogged(LOGGER, "student doesn't exist");
        }
        String fileTitle = student.getFio().split(" ")[0] + "_" + title;
        Document document = documentService.findByTitle(fileTitle.toLowerCase());
        if(document != null){
            return badRequestLogged(LOGGER, "file with such title already exists");
        }

        documentService.save(new Document(studentId, category.toLowerCase(), fileTitle.toLowerCase(), subject, file, isPublic));

        File folder = new File(System.getProperty("user.home") + "\\diploma_docs\\" + studentId);

        if (!folder.exists()) {
            folder.mkdirs();
        }
        file.transferTo(new File(System.getProperty("user.home") + "\\diploma_docs\\" + studentId + "\\"
                + file.getOriginalFilename()));

        return createdLogged(LOGGER, "document was uploaded", null);
    }

    @CrossOrigin
    @RequestMapping(value = "/documentsByStudent/{studentId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity documentsByStudent(@PathVariable Long studentId) throws IOException {
        Student student = studentService.find(studentId);

        if (student == null) {
            return notFoundLogged(LOGGER, "student doesn't exist");
        }

        List<DocumentInfo> documents = documentService.findByStudentId(studentId);

        Map<String, List<DocumentInfo>> groupedDocs =
                documents.stream().collect(Collectors.groupingBy(DocumentInfo::getCategory));

        StudentAndDocInfo info = new StudentAndDocInfo(new StudentBinding(student), groupedDocs);
        return okLogged(LOGGER, "Titles were found", info);
    }

    @CrossOrigin
    @RequestMapping(value = "/documentsByStudents", method = RequestMethod.GET)
    public ResponseEntity documentsByStudents(@RequestParam Long firstStudentId,
                                              @RequestParam Long secondStudentId,
                                              @RequestParam(required = false) Boolean isPublic) {
        Student student = studentService.find(firstStudentId);
        Student student2 = studentService.find(secondStudentId);

        if (isPublic == null)
            isPublic = false;

        if (student == null || student2 == null) {
            return notFoundLogged(LOGGER, "student doesn't exist");
        }

        List<StudentAndDocInfo> docs = new ArrayList<>();

        if (isPublic) {
            List<DocumentInfo> documents = documentService.findAllOpenTitlesByStudentId(firstStudentId);
            List<DocumentInfo> documents2 = documentService.findAllOpenTitlesByStudentId(secondStudentId);

            Map<String, List<DocumentInfo>> groupedDocs =
                    documents.stream().collect(Collectors.groupingBy(DocumentInfo::getCategory));

            Map<String, List<DocumentInfo>> groupedDocs2 =
                    documents2.stream().collect(Collectors.groupingBy(DocumentInfo::getCategory));

            docs.add(new StudentAndDocInfo(new StudentBinding(student), groupedDocs));
            docs.add(new StudentAndDocInfo(new StudentBinding(student2), groupedDocs2));
        } else {
            List<DocumentInfo> documents = documentService.findByStudentId(firstStudentId);
            List<DocumentInfo> documents2 = documentService.findByStudentId(secondStudentId);

            Map<String, List<DocumentInfo>> groupedDocs =
                    documents.stream().collect(Collectors.groupingBy(DocumentInfo::getCategory));

            Map<String, List<DocumentInfo>> groupedDocs2 =
                    documents2.stream().collect(Collectors.groupingBy(DocumentInfo::getCategory));

            docs.add(new StudentAndDocInfo(new StudentBinding(student), groupedDocs));
            docs.add(new StudentAndDocInfo(new StudentBinding(student2), groupedDocs2));
        }

        return okLogged(LOGGER, "Titles were found", docs);
    }

    @CrossOrigin
    @RequestMapping(value = "/checkWithSuperStudent", method = RequestMethod.GET)
    public ResponseEntity checkWithSuperStudent(@RequestParam Long studentId) {
        Student student = studentService.find(studentId);

        if (student == null) {
            return notFoundLogged(LOGGER, "student doesn't exist");
        }
        List<DocumentInfo> documents = documentService.findByStudentId(studentId);
        Student superStudent = studentService.find(SUPERSTUDENT_ID);
        List<DocumentInfo> documents2 = documentService.findByStudentId(SUPERSTUDENT_ID);

        Map<String, List<DocumentInfo>> groupedDocs =
                documents.stream().filter(o -> o.getSubject() != null).collect(Collectors.groupingBy(o -> o.getSubject().toLowerCase()));

        Map<String, List<DocumentInfo>> superStudentGroupedDocs =
                documents2.stream().filter(o -> o.getSubject() != null).collect(Collectors.groupingBy(o -> o.getSubject().toLowerCase()));

        double ratio = 0;

        for (String subject : superStudentGroupedDocs.keySet()) {
            List<DocumentInfo> documentInfos = groupedDocs.get(subject);
            List<DocumentInfo> superDocumentInfos = superStudentGroupedDocs.get(subject);
            ratio += documentInfos == null ? 0 :(double) documentInfos.size() / superDocumentInfos.size();
        }

        double average = ratio / superStudentGroupedDocs.size() * 100;

        List<StudentAndDocInfo> docs = new ArrayList<>();
        docs.add(new StudentAndDocInfo(new StudentBinding(student), groupedDocs));
        docs.add(new StudentAndDocInfo(new StudentBinding(superStudent), superStudentGroupedDocs));
        return okLogged(LOGGER, "Titles were found", new CheckingBinding(docs, average));
    }

    @CrossOrigin
    @RequestMapping(value = "/documentList", method = RequestMethod.GET)
    public ResponseEntity documentList() {
        List<DocumentInfo> documents = documentService.findAllTitles();

        Map<String, List<DocumentInfo>> groupedDocs =
                documents.stream().collect(Collectors.groupingBy(DocumentInfo::getCategory));
        return okLogged(LOGGER, "Titles were found", groupedDocs);
    }

    @CrossOrigin
    @RequestMapping(value = "/openDocumentList", method = RequestMethod.GET)
    public ResponseEntity openDocumentList() {
        List<DocumentInfo> documents = documentService.findAllOpenTitles();

        Map<String, List<DocumentInfo>> groupedDocs =
                documents.stream().collect(Collectors.groupingBy(DocumentInfo::getCategory));

        return okLogged(LOGGER, "Titles were found", groupedDocs);
    }

    @CrossOrigin
    @RequestMapping(value = "/loadDocument/{documentId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity loadDocument(@PathVariable Long documentId) throws IOException {
        Document document = documentService.findById(documentId);

        if (document == null) {
            return notFoundLogged(LOGGER, "document doesn't exist");
        }

        String path = System.getProperty("user.home") + "\\diploma_docs\\" +
                document.getStudentId() + "\\" + document.getFileName();

        Resource file = new PathResource(path);

        if(document.getFileName().endsWith(".doc")){
            WordUtils.readDocFile(path);
        }
        else if(document.getFileName().endsWith(".docx")){
            WordUtils.readDocxFile(path);
        }

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-disposition", "attachment; filename=" + document.getFileName());
        responseHeaders.add("Content-Type", document.getContentType());

        return ResponseEntity
                .ok()
                .headers(responseHeaders)
                .contentLength(file.contentLength())
                .body(new InputStreamResource(file.getInputStream()));
    }

    @CrossOrigin
    @RequestMapping(value = "/loadDocument", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity loadDocument(@RequestParam String title) throws IOException {
        Document document = documentService.findByTitle(title);

        if (document == null) {
            return notFoundLogged(LOGGER, "document doesn't exist");
        }
        HttpHeaders responseHeaders = new HttpHeaders();

        responseHeaders.add("Content-Disposition", "attachment; filename=" + document.getFileName());
        responseHeaders.add("Content-Type", document.getContentType());

        String path = System.getProperty("user.home") + "\\diploma_docs\\" +
                document.getStudentId() + "\\" + document.getFileName();

        Resource file = new PathResource(path);
        if(document.getFileName().endsWith(".doc")){
            WordUtils.readDocFile(path);
        }
        else if(document.getFileName().endsWith(".docx")){
            WordUtils.readDocxFile(path);
        }


        return ResponseEntity
                .ok()
                .headers(responseHeaders)
                .contentLength(file.contentLength())
                .body(new InputStreamResource(file.getInputStream()));
    }

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Success. No Content"),
            @ApiResponse(code = 400, message = "Indicates the requested file is empty."),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "/getFileIdentity", method = RequestMethod.POST)
    public ResponseEntity checkFileWithTitle(@RequestPart("file") MultipartFile file, @RequestParam String title) throws Exception {
        if (!file.isEmpty() && StringUtils.hasLength(title)) {

            String firstText = new BufferedReader(new InputStreamReader(file.getInputStream(), "UTF-8"))
                    .lines().collect(Collectors.joining("\n"));

            Document doc = documentService.findByTitle(title);
            if (doc == null)
                return notFoundLogged(LOGGER, "document doesn't exist");


            File f2 = new File(System.getProperty("user.home") + "\\diploma_docs\\"
                    + doc.getStudentId() + "\\" + doc.getFileName());

            String secondText = new BufferedReader(new InputStreamReader(new FileInputStream(f2), "UTF-8"))
                    .lines().collect(Collectors.joining("\n"));

            mshComp.processTexts(firstText, secondText);


            ShinglesResult result = new ShinglesResult(shComp.getHashCount(), shComp.getResult() * 100, sshComp.getResult(), mshComp.getResult());

            LOGGER.info("\tHashCount==" + shComp.getHashCount());
            LOGGER.info("\tPercentage of matching shingles:  " + shComp.getResult() * 100 + "%");
            LOGGER.info("\tAmount of matching super shingles: " + sshComp.getResult());
            LOGGER.info("\tAmount of matching mega shingles: " + mshComp.getResult());

            return okLogged(LOGGER, "files were checked", result);
        } else {
            return badRequestLogged(LOGGER, "Error while uploading");
        }
    }

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Success. No Content"),
            @ApiResponse(code = 400, message = "Indicates the requested file is empty."),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "/titles/getFileIdentity", method = RequestMethod.POST)
    public ResponseEntity checkFiles(@RequestParam String firstTitle, @RequestParam String secondTitle) throws Exception {

        if (StringUtils.hasLength(firstTitle) && StringUtils.hasLength(secondTitle)) {
            Document doc = documentService.findByTitle(firstTitle);
            Document doc2 = documentService.findByTitle(secondTitle);

            if (doc == null || doc2 == null)
                return notFoundLogged(LOGGER, "document doesn't exist");


            File f1 = new File(System.getProperty("user.home") + "\\diploma_docs\\"
                    + doc.getStudentId() + "\\" + doc.getFileName());

            String firstText = new BufferedReader(new InputStreamReader(new FileInputStream(f1), "UTF-8"))
                    .lines().collect(Collectors.joining("\n"));


            File f2 = new File(System.getProperty("user.home") + "\\diploma_docs\\"
                    + doc2.getStudentId() + "\\" + doc2.getFileName());

            String secondText = new BufferedReader(new InputStreamReader(new FileInputStream(f2), "UTF-8"))
                    .lines().collect(Collectors.joining("\n"));

            mshComp.processTexts(firstText, secondText);


            ShinglesResult result = new ShinglesResult(shComp.getHashCount(), shComp.getResult() * 100, sshComp.getResult(), mshComp.getResult());

            LOGGER.info("\tHashCount==" + shComp.getHashCount());
            LOGGER.info("\tPercentage of matching shingles:  " + shComp.getResult() * 100 + "%");
            LOGGER.info("\tAmount of matching super shingles: " + sshComp.getResult());
            LOGGER.info("\tAmount of matching mega shingles: " + mshComp.getResult());

            return okLogged(LOGGER, "files were checked", result);
        } else {
            return badRequestLogged(LOGGER, "Error while uploading");
        }
    }
}
