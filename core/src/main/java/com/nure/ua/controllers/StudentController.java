package com.nure.ua.controllers;

import com.nure.ua.bindings.Doc;
import com.nure.ua.bindings.ShinglesResult;
import com.nure.ua.bindings.StudentBinding;
import com.nure.ua.model.Student;
import com.nure.ua.model.shingles.MegaShinglesComparer;
import com.nure.ua.model.shingles.ShinglesComparer;
import com.nure.ua.model.shingles.SuperShinglesComparer;
import com.nure.ua.morpher.NameReflect;
import com.nure.ua.services.StudentService;
import com.nure.ua.xls.Parser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.security.KeyStore;
import java.util.stream.Collectors;

import static com.nure.ua.util.LoggingResponse.*;
import static org.slf4j.LoggerFactory.getLogger;

@Api(value = "/student", description = "Operations with student")
@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private Parser parser;

    @Autowired
    private ShinglesComparer shComp;

    @Autowired
    private SuperShinglesComparer sshComp;

    @Autowired
    private MegaShinglesComparer mshComp;

    private static final org.slf4j.Logger LOGGER = getLogger(StudentController.class);

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success. Returns student", response = StudentBinding.class),
            @ApiResponse(code = 404, message = "Indicates the requested student was not found."),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity getStudent(@PathVariable(value = "id") Long id) {
        Student student = studentService.find(id);
        if (student != null) {
            StudentBinding binding = new StudentBinding(student);
            return okLogged(LOGGER, "student has been found", binding);
        } else {
            return notFoundLogged(LOGGER, "student does not exist");
        }
    }

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success. Returns students document", response = Doc.class),
            @ApiResponse(code = 404, message = "Indicates the requested student was not found."),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "{id}/doc", method = RequestMethod.GET)
    public ResponseEntity getStudentDoc(@PathVariable(value = "id") Long id) {
        Student student = studentService.find(id);
        if (student != null) {
            NameReflect nameReflect = new NameReflect(student.getFio());
            Doc doc = new Doc("Справка дана " + nameReflect.getCaseList(NameReflect.UADAVALNYI) + " о том что он(она) действительно является студентом "
                    + student.getCourse() + "-го курса дневной формы обучения.");
            return okLogged(LOGGER, "document has been generated", doc);
        } else {
            return notFoundLogged(LOGGER, "student does not exist");
        }
    }

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Success. No Content"),
            @ApiResponse(code = 400, message = "Indicates the requested file is empty."),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "/parse", method = RequestMethod.POST)
    public ResponseEntity uploadFile(@RequestPart("file") MultipartFile file) throws IOException {
        if (!file.isEmpty()) {
            parser.parse(file.getOriginalFilename(), file.getInputStream());
            return okLogged(LOGGER, "file was uploaded", null);
        } else {
            return badRequestLogged(LOGGER, "Error while uploading");
        }
    }


    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Success. Returned generated file"),
            @ApiResponse(code = 400, message = "Indicates the requested file is empty."),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "/create_certificate", method = RequestMethod.POST)
    public ResponseEntity createJksFile(@RequestBody String password) throws Exception {
        try {
            File file = new File(System.getProperty("user.home") + "\\jks");

            if (!file.exists()) {
                file.mkdirs();
            }

            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(null, password.toCharArray());
            // Store away the keystore.

            file = new File(System.getProperty("user.home") + "\\jks\\key.jks");
            if (file.exists()) {
                file.delete();
            }

            FileOutputStream fos = new FileOutputStream(System.getProperty("user.home")
                    + "\\jks\\" + "key.jks");
            ks.store(fos, password.toCharArray());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return okLogged(LOGGER, "student has been found", "http://localhost:8080/student/create_certificate/key.jks");
    }

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Success. Returned generated file"),
            @ApiResponse(code = 400, message = "Indicates the requested file is empty."),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "/create_certificate/key.jks", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity uploadJksFile() throws Exception {
        Resource jksFile = new PathResource(System.getProperty("user.home") + "\\jks\\" + "key.jks");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment");
        headers.add("filename=", "key.jks");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(jksFile.contentLength())
                .body(new InputStreamResource(jksFile.getInputStream()));
    }

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Success. No Content"),
            @ApiResponse(code = 400, message = "Indicates the requested file is empty."),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "/getFilesIdentity", method = RequestMethod.POST)
    public ResponseEntity checkFiles(@RequestPart("first") MultipartFile first, @RequestPart("second") MultipartFile second) throws Exception {
        if (!first.isEmpty() && !second.isEmpty()) {

            String firstText = new BufferedReader(new InputStreamReader(first.getInputStream(), "UTF-8"))
                    .lines().collect(Collectors.joining("\n"));

            String secondText = new BufferedReader(new InputStreamReader(second.getInputStream(), "UTF-8"))
                    .lines().collect(Collectors.joining("\n"));
            mshComp.processTexts(firstText, secondText);


            ShinglesResult result = new ShinglesResult(shComp.getHashCount(), shComp.getResult() * 100, sshComp.getResult(), mshComp.getResult());

            LOGGER.info("\tHashCount==" + shComp.getHashCount());
            LOGGER.info("\tPercentage of matching shingles:  " + shComp.getResult() * 100 + "%");
            LOGGER.info("\tAmount of matching super shingles: " + sshComp.getResult());
            LOGGER.info("\tAmount of matching mega shingles: " + mshComp.getResult());

            return okLogged(LOGGER, "files were checked", result);
        } else {
            return badRequestLogged(LOGGER, "Error while uploading");
        }
    }
}
