package com.nure.ua.controllers;

import com.nure.ua.bindings.BlockChainBinding;
import com.nure.ua.bindings.Doc;
import com.nure.ua.model.blockchain.Block;
import com.nure.ua.model.blockchain.BlockChainData;
import com.nure.ua.services.BlockChainDataService;
import com.nure.ua.services.BlockChainService;
import com.nure.ua.services.ChainCheck;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.nure.ua.util.LoggingResponse.*;
import static org.slf4j.LoggerFactory.getLogger;

@Api(value = "/blockchain", description = "Operations with blockchain")
@RestController
@RequestMapping("/blockchain")
public class BlockChainController {

    @Autowired
    private BlockChainService blockchainService;

    @Autowired
    private BlockChainDataService blockChainDataService;

    private static final org.slf4j.Logger LOGGER = getLogger(BlockChainController.class);

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success. Returns blockchain", response = BlockChainBinding.class),
            @ApiResponse(code = 404, message = "Indicates the requested blockchain was not found."),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity getBlockChain(@PathVariable(value = "id") Long id) {
        Block block = blockchainService.find(id);
        if (block != null) {
            BlockChainBinding binding = new BlockChainBinding(block);
            return okLogged(LOGGER, "Blockchain has been found", binding);
        } else {
            return notFoundLogged(LOGGER, "Blockchain does not exist");
        }
    }

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success. Returns blockchain", response = BlockChainBinding.class),
            @ApiResponse(code = 404, message = "Indicates the requested blockchain was not found."),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "/hash/{hash}", method = RequestMethod.GET)
    public ResponseEntity getBlockchainByHash(@PathVariable(value = "hash") String hash) {
        Block block = blockchainService.findByHash(hash);
        if (block != null) {
            BlockChainBinding binding = new BlockChainBinding(block);
            return okLogged(LOGGER, "Blockchain has been found", binding);
        } else {
            return notFoundLogged(LOGGER, "Blockchain does not exist");
        }
    }

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Success. Returns is blockChain valid or not", response = String.class),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "/checkHashes", method = RequestMethod.GET)
    public ResponseEntity checkBlockChainHashes(@RequestParam List<String> hashes) {
        List<Block> blocks = new ArrayList<>();
        for (String hash : hashes) {
            Block block = blockchainService.findByHash(hash);
            if (block != null) {
                blocks.add(block);
            }
        }
        if(blocks.size() < 2){
            return okLogged(LOGGER, "Blockchain hashes are not valid", new Doc("There is only one " +
                    "hash presented in our system"));
        }
        Boolean chainValid = ChainCheck.isChainValid(blocks);
        if (chainValid) {
            return okLogged(LOGGER, "Blockchain hashes are valid", new Doc("Blockchain hashes are valid"));
        } else {
            return okLogged(LOGGER, "Blockchain hashes are not valid", new Doc("Blockchain hashes are not valid"));
        }
    }

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Success. Creates a blockchain"),
            @ApiResponse(code = 400, message = "Indicates the requested blockchain is invalid."),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity createBlockChain(@RequestBody BlockChainBinding blockchain) {
        Block block = blockchainService.save(new Block(blockchain));

        for (BlockChainData data : block.getData()) {
            BlockChainData blockChainData = blockChainDataService.find(data.getId());
            blockChainData.setBlockchain_id(block.getId());
            blockChainDataService.updateBlockChainData(blockChainData);

        }
        return createdLogged(LOGGER, "Blockchain has been created", block);
    }

    @CrossOrigin
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success. Returns blockchain", response = BlockChainBinding.class),
            @ApiResponse(code = 404, message = "Indicates the requested blockchain was not found."),
            @ApiResponse(code = 500, message = "Failure. Unexpected condition was encountered.")})
    @RequestMapping(value = "/hash", method = RequestMethod.GET)
    public ResponseEntity getBlockchainByHashes(@RequestParam List<String> hashes) {
        List<BlockChainBinding> bindings = new ArrayList<>();
        for (String hash : hashes) {
            Block block = blockchainService.findByHash(hash);
            if (block != null) {
                BlockChainBinding binding = new BlockChainBinding(block);
                bindings.add(binding);
            }
        }
        if (!CollectionUtils.isEmpty(bindings)) {
            return okLogged(LOGGER, "Blockchains have been found", bindings);
        } else {
            return notFoundLogged(LOGGER, "Blockchains does not exist");
        }
    }
}
