package com.nure.ua.bindings;

public class Doc {

    private String document;

    public Doc() {
    }

    public Doc(String document) {
        this.document = document;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }
}
