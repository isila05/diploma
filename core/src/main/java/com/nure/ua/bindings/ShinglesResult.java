package com.nure.ua.bindings;

public class ShinglesResult {

    private int hashCount;

    private double matchingResult;

    private double superShingles;

    private double megaShingles;

    public ShinglesResult(int hashCount, double matchingResult, double superShingles, double megaShingles) {
        this.hashCount = hashCount;
        this.matchingResult = matchingResult;
        this.superShingles = superShingles;
        this.megaShingles = megaShingles;
    }

    public int getHashCount() {
        return hashCount;
    }

    public void setHashCount(int hashCount) {
        this.hashCount = hashCount;
    }

    public double getMatchingResult() {
        return matchingResult;
    }

    public void setMatchingResult(double matchingResult) {
        this.matchingResult = matchingResult;
    }

    public double getSuperShingles() {
        return superShingles;
    }

    public void setSuperShingles(double superShingles) {
        this.superShingles = superShingles;
    }

    public double getMegaShingles() {
        return megaShingles;
    }

    public void setMegaShingles(double megaShingles) {
        this.megaShingles = megaShingles;
    }
}
