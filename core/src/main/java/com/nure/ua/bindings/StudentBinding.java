package com.nure.ua.bindings;

import com.nure.ua.model.Student;

public class StudentBinding {

    private String fio;

    private String group;

    private String studentBook;

    private String email;

    private Integer groupNumber;

    private Integer course;

    private Boolean isOnBudget;

    public StudentBinding() {
    }

    public StudentBinding(String fio, String group, String studentBook, String email, Integer groupNumber, Integer course, Boolean isOnBudget) {
        this.fio = fio;
        this.group = group;
        this.studentBook = studentBook;
        this.email = email;
        this.groupNumber = groupNumber;
        this.course = course;
        this.isOnBudget = isOnBudget;
    }

    public StudentBinding(Student student) {
        this.fio = student.getFio();
        this.group = student.getStudentGroup();
        this.studentBook = student.getStudentBook();
        this.email = student.getEmail();
        this.groupNumber = student.getGroupNumber();
        this.course = student.getCourse();
        this.isOnBudget = student.getOnBudget();
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public Integer getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(Integer groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getStudentBook() {
        return studentBook;
    }

    public void setStudentBook(String studentBook) {
        this.studentBook = studentBook;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getNumber() {
        return groupNumber;
    }

    public void setNumber(Integer number) {
        this.groupNumber = number;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Boolean getOnBudget() {
        return isOnBudget;
    }

    public void setOnBudget(Boolean onBudget) {
        isOnBudget = onBudget;
    }
}
