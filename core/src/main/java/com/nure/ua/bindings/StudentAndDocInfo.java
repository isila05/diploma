package com.nure.ua.bindings;

import java.util.List;
import java.util.Map;

public class StudentAndDocInfo {
    private StudentBinding student;
    private Map<String, List<DocumentInfo>> documents;

    public StudentAndDocInfo() {
    }

    public StudentAndDocInfo(StudentBinding student, Map<String, List<DocumentInfo>> documents) {
        this.student = student;
        this.documents = documents;
    }

    public StudentBinding getStudent() {
        return student;
    }

    public void setStudent(StudentBinding student) {
        this.student = student;
    }

    public Map<String, List<DocumentInfo>> getDocuments() {
        return documents;
    }

    public void setDocuments(Map<String, List<DocumentInfo>> documents) {
        this.documents = documents;
    }
}

