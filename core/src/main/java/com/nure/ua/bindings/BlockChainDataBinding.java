package com.nure.ua.bindings;

import com.nure.ua.model.blockchain.BlockChainData;

import java.time.LocalDate;

public class BlockChainDataBinding {

    private String title;
    private String creationDate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public BlockChainDataBinding(String title, String creationDate) {
        this.title = title;
        this.creationDate = creationDate;
    }

    public BlockChainDataBinding(BlockChainData blockchainData) {
        this.title = blockchainData.getTitle();
        this.creationDate = blockchainData.getCreationDate().toString();
    }

    public BlockChainDataBinding() {
    }

}
