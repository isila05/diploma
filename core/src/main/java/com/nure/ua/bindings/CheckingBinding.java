package com.nure.ua.bindings;

import java.util.List;

public class CheckingBinding {

    List<StudentAndDocInfo> documents;

    double percentage;

    public CheckingBinding(List<StudentAndDocInfo> documents, double percentage) {
        this.documents = documents;
        this.percentage = percentage;
    }

    public List<StudentAndDocInfo> getDocuments() {
        return documents;
    }

    public void setDocuments(List<StudentAndDocInfo> documents) {
        this.documents = documents;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }
}
