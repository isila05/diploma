package com.nure.ua.bindings;

import com.nure.ua.model.blockchain.Block;
import com.nure.ua.model.blockchain.BlockChainData;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BlockChainBinding {

    private String hash;

    private String previousHash;

    private String date;

    private List<BlockChainDataBinding> blockchainData;

    public BlockChainBinding(String hash, String previousHash, String date, List<BlockChainDataBinding> blockchainData) {
        this.hash = hash;
        this.previousHash = previousHash;
        this.date = date;
        this.blockchainData = blockchainData;
    }

    public BlockChainBinding(List<BlockChainDataBinding> blockchainData) {
        this.blockchainData = blockchainData;
    }

    public BlockChainBinding() {}

    public BlockChainBinding(Block block){
        this.hash = block.getHash();
        this.previousHash = block.getPreviousHash();
        this.date = block.getDate().toString();
        List<BlockChainDataBinding> dataBindings = new ArrayList<>();
        for (BlockChainData data : block.getData()) {
            dataBindings.add(new BlockChainDataBinding(data));
        }
        this.blockchainData = dataBindings;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<BlockChainDataBinding> getBlockchainData() {
        return blockchainData;
    }

    public void setBlockchainData(List<BlockChainDataBinding> blockchainData) {
        this.blockchainData = blockchainData;
    }
}
