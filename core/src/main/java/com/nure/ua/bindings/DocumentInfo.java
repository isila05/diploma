package com.nure.ua.bindings;

import com.nure.ua.model.Document;

public class DocumentInfo {

    private String category;
    private String subject;
    private String title;

    public DocumentInfo(Document document) {
        this.category = document.getCategory();
        this.subject = document.getSubject();
        this.title = document.getTitle();
    }

    public DocumentInfo(Object[] objects) {
        this.category = objects[1] != null ? objects[1].toString() : null;
        this.subject = objects[2] != null ?  objects[2].toString() : null;
        this.title = objects[0] != null ?  objects[0].toString() : null;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
