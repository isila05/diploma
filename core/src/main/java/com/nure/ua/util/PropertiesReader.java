package com.nure.ua.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.slf4j.LoggerFactory.getLogger;

public class PropertiesReader {

    private static final org.slf4j.Logger LOGGER = getLogger(PropertiesReader.class);

    public static Properties getProperties(String propertiesFileName) {
        Properties properties = new Properties();

        try (InputStream is = PropertiesReader.class.getClassLoader().getResourceAsStream(propertiesFileName)) {
            if (is != null) {
                properties.load(is);
            } else {
                throw new FileNotFoundException("Property file " + propertiesFileName + " was not found in classpath");
            }
        } catch (IOException ex) {
            LOGGER.error("Exception occurred while reading {} file: {}", propertiesFileName, ex.getMessage());
        }

        return properties;
    }
}
