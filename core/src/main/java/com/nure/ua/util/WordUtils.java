package com.nure.ua.util;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.FileInputStream;

public class WordUtils {

    public static void readDocFile(String file) {
        try {
            FileInputStream fis = new FileInputStream(file);

            HWPFDocument doc = new HWPFDocument(fis);

            WordExtractor we = new WordExtractor(doc);

            //we.getText();
            //todo: return stream to UI
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void readDocxFile(String file) {

        try {
            FileInputStream fis = new FileInputStream(file);

            XWPFDocument xdoc = new XWPFDocument(fis);
            XWPFWordExtractor extractor = new XWPFWordExtractor(xdoc);
            //extractor.getText();
            //todo: return stream to UI


            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
