package com.nure.ua.util;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class LoggingResponse {
    public static ResponseEntity okLogged(Logger logger, String message, Object body) {
        logger.info(message);
        return ResponseEntity.ok(body);
    }

    public static ResponseEntity createdLogged(Logger logger, String message, Object body) {
        logger.info(message);
        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    public static ResponseEntity badRequestLogged(Logger logger, String message) {
        logger.warn(message);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Gson().toJson(message));
    }

    public static ResponseEntity notFoundLogged(Logger logger, String message) {
        logger.warn(message);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public static ResponseEntity serviceUnavailableLogged(Logger logger, String message) {
        logger.error(message);
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }

    public static ResponseEntity internalServerErrorLogged(Logger logger, String message) {
        logger.error(message);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
