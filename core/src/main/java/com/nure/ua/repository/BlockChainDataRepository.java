package com.nure.ua.repository;

import com.nure.ua.model.blockchain.BlockChainData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlockChainDataRepository extends CrudRepository<BlockChainData, Long> {
}
