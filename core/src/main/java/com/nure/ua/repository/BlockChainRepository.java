package com.nure.ua.repository;

import com.nure.ua.model.blockchain.Block;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlockChainRepository extends CrudRepository<Block, Long> {

    Block findByHash(String hash);
}
