package com.nure.ua.repository;

import com.nure.ua.model.Document;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends CrudRepository<Document, Long> {

    @Query(value = "select d.title, d.category, d.subject from document d where d.studentId =:studentId", nativeQuery = true)
    List<Object[]> findAllByStudentId(@Param("studentId") Long studentId);

    @Query(value = "select d.title, d.category, d.subject from document d", nativeQuery = true)
    List<Object[]> findAllTitles();

    @Query(value = "select d.title, d.category, d.subject from document d where d.isPublic = true", nativeQuery = true)
    List<Object[]> findAllOpenFileTitles();

    @Query(value = "selectd.title, d.category, d.subject from document d where d.isPublic = true and d.studentId =:studentId", nativeQuery = true)
    List<Object[]> findAllOpenFileTitlesByStudent(@Param("studentId") Long studentId);

    Document findByTitle(String title);
}
