package com.nure.ua.services;

import com.nure.ua.model.blockchain.Block;
import com.nure.ua.model.blockchain.BlockChainData;
import com.nure.ua.repository.BlockChainDataRepository;
import com.nure.ua.repository.BlockChainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BlockChainDataService {

    @Autowired
    private BlockChainDataRepository blockChainDataRepository;

    @Transactional
    public BlockChainData save(BlockChainData blockChainData) {
        return blockChainDataRepository.save(blockChainData);
    }

    public BlockChainData find(long blockChainDataId) {
        return blockChainDataRepository.findOne(blockChainDataId);
    }

    @Transactional
    public BlockChainData updateBlockChainData(BlockChainData blockChainData) {
        return blockChainDataRepository.save(blockChainData);
    }

    @Transactional
    public void delete(BlockChainData block) {
        blockChainDataRepository.delete(block);
    }

    public List<BlockChainData> getAllBlocks() {
        return (List<BlockChainData>) blockChainDataRepository.findAll();
    }
}
