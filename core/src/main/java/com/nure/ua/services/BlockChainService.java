package com.nure.ua.services;

import com.nure.ua.model.blockchain.Block;
import com.nure.ua.repository.BlockChainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BlockChainService {

    @Autowired
    private BlockChainRepository blockChainRepository;

    @Transactional
    public Block save(Block block) {
        return blockChainRepository.save(block);
    }

    public Block find(long blockId) {
        return blockChainRepository.findOne(blockId);
    }

    public Block findByHash(String hash) {
        return blockChainRepository.findByHash(hash);
    }

    @Transactional
    public Block updateUser(Block block) {
        return blockChainRepository.save(block);
    }

    @Transactional
    public void delete(Block block) {
        blockChainRepository.delete(block);
    }

    public List<Block> getAllBlocks() {
        return (List<Block>) blockChainRepository.findAll();
    }
}
