package com.nure.ua.services;

import com.nure.ua.model.Student;
import com.nure.ua.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Transactional
    public Student save(Student student) {
        return studentRepository.save(student);
    }

    public Student find(long studentId) {
        return studentRepository.findOne(studentId);
    }

    @Transactional
    public Student updateUser(Student student) {
        return studentRepository.save(student);
    }

    public Student findByFio(String fio) {
        return studentRepository.findByFio(fio);
    }

    @Transactional
    public void delete(Student student) {
        studentRepository.delete(student);
    }

    public List<Student> getAllUsers() {
        return (List<Student>) studentRepository.findAll();
    }
}
