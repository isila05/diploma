package com.nure.ua.services;

import com.nure.ua.bindings.DocumentInfo;
import com.nure.ua.model.Document;
import com.nure.ua.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    @Transactional
    public Document save(Document document) {
        return documentRepository.save(document);
    }

    public Document findById(long documentId) {
        return documentRepository.findOne(documentId);
    }

    public Document findByTitle(String title) {
        return documentRepository.findByTitle(title);
    }

    @Transactional
    public Document updateDocument(Document document) {
        return documentRepository.save(document);
    }

    public List<DocumentInfo> findByStudentId(long studentId) {
        List<Object[]> objects = documentRepository.findAllByStudentId(studentId);
        return objects.stream().map(DocumentInfo::new).collect(Collectors.toList());
    }

    public List<DocumentInfo> findAllTitles() {
        List<Object[]> titles = documentRepository.findAllTitles();
        return titles.stream().map(DocumentInfo::new).collect(Collectors.toList());
    }

    public List<DocumentInfo> findAllOpenTitles() {
        List<Object[]> openFileTitles = documentRepository.findAllOpenFileTitles();
        return openFileTitles.stream().map(DocumentInfo::new).collect(Collectors.toList());
    }

    public List<DocumentInfo> findAllOpenTitlesByStudentId(long studentId) {
        List<Object[]> titles = documentRepository.findAllOpenFileTitlesByStudent(studentId);
        return titles.stream().map(DocumentInfo::new).collect(Collectors.toList());
    }

    @Transactional
    public void delete(Document document) {
        documentRepository.delete(document);
    }


}
