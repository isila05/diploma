package com.nure.ua.xls;

import com.nure.ua.model.Student;
import com.nure.ua.services.StudentService;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@Component
public class Parser {

    @Autowired
    StudentService studentService;

    private static final String NUMBER = "№";
    private static final String FIO = "Прізвище, ім'я, по батькові";
    private static final String STUDENT_BOOK = "№ залікової книжки";
    private static final String BUDGET = "Контракт";
    private static final String FOREIGN = "Іноземець";
    private static final String NOTE = "Примітка";
    private static String group;


    public void parse(String name, InputStream stream) throws IOException {
        if (name.endsWith("xlsx")) {
            XSSFWorkbook myExcelBook = new XSSFWorkbook(stream);
            XSSFSheet myExcelSheet = myExcelBook.getSheetAt(0);
            for (Row row : myExcelSheet) {
                workWithCells(row);
            }
            myExcelBook.close();
        } else {
            HSSFWorkbook myExcelBook = new HSSFWorkbook(stream);
            HSSFSheet myExcelSheet = myExcelBook.getSheetAt(0);
            for (Row row : myExcelSheet) {
                workWithCells(row);
            }
            myExcelBook.close();
        }
    }

    private boolean isNotInfo(Cell cell) {
        return (cell.toString().length() < 1) || isNotHeader(cell);
    }

    private static boolean isNotHeader(Cell cell) {
        return Objects.equals(cell.toString(), NUMBER) ||
                Objects.equals(cell.toString(), FIO) ||
                Objects.equals(cell.toString(), STUDENT_BOOK) ||
                Objects.equals(cell.toString(), BUDGET) ||
                Objects.equals(cell.toString(), FOREIGN) ||
                Objects.equals(cell.toString(), NOTE);
    }

    private void workWithCells(Row row) {
        for (Cell cell : row) {
            if (!isNotInfo(cell)) {
                if (row.getCell(0).toString().length() > 0 && row.getCell(1).toString().length() > 0) {
                    Student student = studentService.findByFio(row.getCell(1).toString());
                    if(student == null){
                        studentService.save(buildStudent(new Student(), row));
                    }
                    else{
                        studentService.updateUser(buildStudent(student, row));
                    }
                } else {
                    group = row.getCell(0).toString();
                }
                break;
            }
        }
    }

    private Student buildStudent(Student student, Row row){
        student.setGroupNumber(Integer.parseInt(group.substring(group.length() - 1)));
        student.setStudentGroup(group);
        student.setFio(row.getCell(1).toString());
        student.setOnBudget(row.getCell(3).toString().length() > 0);
        student.setStudentBook(row.getCell(2).toString());
        student.setCourse(parseCourse());
        return student;
    }

    private Integer parseCourse() {
        Calendar now = Calendar.getInstance();
        Calendar september = Calendar.getInstance();
        september.set(now.get(Calendar.YEAR), Calendar.SEPTEMBER, 1);

        if (now.after(september)) {
            if (group.contains("с") || group.contains("м")) {
                return now.get(Calendar.YEAR) - 1995 - Integer.parseInt(group.substring(group.length() - 4, group.length() - 2));
            }
            return now.get(Calendar.YEAR) - 2000 - Integer.parseInt(group.substring(group.length() - 4, group.length() - 2));
        } else {
            if (group.contains("с") || group.contains("м")) {
                return now.get(Calendar.YEAR) - 1996 - Integer.parseInt(group.substring(group.length() - 4, group.length() - 2));
            }
            return now.get(Calendar.YEAR) - 2001 - Integer.parseInt(group.substring(group.length() - 4, group.length() - 2));
        }
    }
}
