CREATE TABLE IF NOT EXISTS "student" (
  id                  BIGSERIAL PRIMARY KEY UNIQUE NOT NULL,
  fio                 CHARACTER VARYING,
  studentgroup        CHARACTER VARYING,
  studentbook         CHARACTER VARYING,
  email               CHARACTER VARYING,
  groupnumber         INTEGER,
  course              INTEGER,
  isonbudget          BOOLEAN
);

CREATE TABLE IF NOT EXISTS "document" (
  id                  BIGSERIAL PRIMARY KEY UNIQUE NOT NULL,
  studentId           BIGSERIAL NOT NULL REFERENCES student(id),
  category            CHARACTER VARYING,
  title               CHARACTER VARYING,
  fileName            CHARACTER VARYING,
  isPublic            BOOLEAN DEFAULT TRUE,
  subject             CHARACTER VARYING DEFAULT NULL,
  contentType         CHARACTER VARYING
);

CREATE TABLE IF NOT EXISTS "blockchain" (
  id                  BIGSERIAL PRIMARY KEY UNIQUE NOT NULL,
  hash                CHARACTER VARYING NOT NULL,
  previousHash        CHARACTER VARYING,
  date                TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "blockchain_data" (
  id                  BIGSERIAL PRIMARY KEY UNIQUE NOT NULL,
  blockchain_id       BIGSERIAL REFERENCES blockchain(id),
  title               CHARACTER VARYING NOT NULL,
  creationDate        TIMESTAMP
);

ALTER TABLE "blockchain_data" ALTER COLUMN blockchain_id DROP NOT NULL;

INSERT INTO "student" VALUES (1, 'super student KTURE', 'СКСм-16-1', '16.123СКСм.0122', 'superstudent@gmail.com', 1, 6, true);
INSERT INTO "document"(id, studentid, category, title, filename, ispublic, subject, contenttype) VALUES
  (1, 1, 'labs', 'Programming_lb1', 'Programming_lb1.docx', true, 'Programming', 'docx'),
  (2, 1, 'labs', 'Programming_lb2', 'Programming_lb2.docx', true, 'Programming', 'docx'),
  (3, 1, 'labs', 'Programming_lb3', 'Programming_lb3.docx', true, 'Programming', 'docx'),
  (4, 1, 'labs', 'Programming_lb4', 'Programming_lb4.docx', true, 'Programming', 'docx'),
  (5, 1, 'labs', 'Programming_lb5', 'Programming_lb5.docx', true, 'Programming', 'docx'),
  (6, 1, 'labs', 'Cloud_lb1', 'Cloud_lb1.docx', true, 'Cloud', 'docx'),
  (7, 1, 'labs', 'Cloud_lb2', 'Cloud_lb2.docx', true, 'Cloud', 'docx'),
  (8, 1, 'labs', 'Cloud_lb3', 'Cloud_lb3.docx', true, 'Cloud', 'docx'),
  (9, 1, 'labs', 'Cloud_lb4', 'Cloud_lb4.docx', true, 'Cloud', 'docx'),
  (10, 1, 'labs', 'Math1', 'Math_lb1.docx', true, 'Math', 'docx'),
  (11, 1, 'labs', 'Math2', 'Math_lb2.docx', true, 'Math', 'docx'),
  (12, 1, 'labs', 'Math3', 'Math_lb3.docx', true, 'Math', 'docx'),
  (13, 1, 'labs', 'Math4', 'Math_lb4.docx', true, 'Math', 'docx'),
  (14, 1, 'theses', 'Theses1', 'Theses1.docx', true, null, 'docx'),
  (15, 1, 'theses', 'Theses2', 'Theses2.docx', true, null, 'docx'),
  (16, 1, 'courseProject', 'Course_project_student', 'Course_project_student.docx', true, 'Programming', 'docx');

ALTER SEQUENCE student_id_seq RESTART WITH 2;

ALTER SEQUENCE document_id_seq RESTART WITH 17;