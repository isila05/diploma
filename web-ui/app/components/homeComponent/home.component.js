'use strict';
(function(){
    angular.module('dip.directives')
        .directive('homeComponent',homeComponent)
    function homeComponent(){
        return {
            scope: {},
            restrict:"E",
            bindToController: true,
            controller: homeComponentCtrl,
            controllerAs: '$ctrl',
            templateUrl: 'components/homeComponent/homeComponent.html',
        }
    }
    homeComponentCtrl.$inject=['$scope','$timeout','$http','$rootScope','$q', 'toaster']
    function homeComponentCtrl($scope,$timeout,$http,$rootScope,$q, toaster) {
        var $ctrl = this;
        $ctrl.studentId = "";
        $ctrl.newDocumentData = {};
        $ctrl.docementCategories = [
            {name:"Отчет по лабораторной работе", val:"labs"},
            {name:"Медицинская справка", val:"medical"},
            {name:"Тезисы", val:"theses"},
            {name:"Курсовой проект", val:"courseProject"},
            {name:"Другое", val:"other"},];
        $ctrl.downloadDocumentTitle='';
        $ctrl.getStudentData = getStudentData;
        $ctrl.getStudentDocuments = getStudentDocuments;
        $ctrl.newDocument = newDocument;
        $ctrl.downloadDocument = downloadDocument;
        $ctrl.downloadDocumentByTitle=downloadDocumentByTitle;
        $ctrl.uploadDocument = uploadDocument;
        function getStudentData(){
            if(!$ctrl.studentId){
                toaster.pop('warning', "Введите ID студента");
                return
            }
            $ctrl.showNewDocumentForm = false;
            $http.get('http://localhost:8080/student/' + $ctrl.studentId, {

            }).then(function(response){
                console.log(response.data)
                $ctrl.studentDocuments = null;
                $ctrl.studetData=response.data;
            }).catch(function (err) {
                console.log(err)
            });
        }
        function getStudentDocuments(){
            if(!$ctrl.studentId){
                toaster.pop('warning', "Введите ID студента");
                return
            }
            $ctrl.showNewDocumentForm = false;
            $http.get('http://localhost:8080/document/documentsByStudent/' + $ctrl.studentId, {

            }).then(function(response){
                console.log(response.data)
                $ctrl.studetData = null;
                $ctrl.studentDocuments=response.data;
            }).catch(function (err) {
                console.log(err)

            });
        }
        function newDocument() {
            $ctrl.showNewDocumentForm = !$ctrl.showNewDocumentForm;
            if($ctrl.showNewDocumentForm){
                $ctrl.showDownloadDocumentForm=false;
            }
        }
        function downloadDocument() {
            $ctrl.showDownloadDocumentForm = !$ctrl.showDownloadDocumentForm;
            if($ctrl.showDownloadDocumentForm){
                $ctrl.showNewDocumentForm=false;
            }
        }
        function downloadDocumentByTitle() {
            var url =  'http://localhost:8080/document/loadDocument?title=' + $ctrl.downloadDocumentTitle;
            $http.get(url).then(function(result){
                    console.log(result)
                },
                function(err){
                    console.log(err)
                    toaster.pop('error', "Ошибкаnp,m скачивания документа");
                });
        }
        function getGetRequest(data) {
            var s='';
            if(data.documentTitle){
                if(!s){s+='?'}else{s+='&'}
                s+='title='+data.documentTitle
            }else{
                return {fieldName:'Заголовок документа'};
            }
            if(data.documentSubject){
                if(!s){s+='?'}else{s+='&'}
                s+='subject='+data.documentSubject
            }
            if(data.documentCategory){
                if(!s){s+='?'}else{s+='&'}
                s+='category='+data.documentCategory
            }else{
                return {fieldName:'Категорию документа'};гш
            }
            if(data.isPublic){
                if(!s){s+='?'}else{s+='&'}
                s+='isPublic='+data.isPublic
            }else {
                if(!s){s+='?'}else{s+='&'}
                s+='isPublic=false'
            }
            return s;
        }
        function uploadDocument() {
            if(!$ctrl.newDocumentData.documentStudentId){
                //toaster.pop('warning', "Заполните поля",'ID студента');
                toaster.pop('warning', "Заполните поля");
                return;
            }
            if($ctrl.myFile){
                var fd = new FormData();
                fd.append('file', $ctrl.myFile);
            }else{
                toaster.pop('warning', "Не выбран документ");
                return;
            }

            var urlSuffix = getGetRequest($ctrl.newDocumentData);

            if(urlSuffix && typeof urlSuffix=='object'){
                //toaster.pop('warning', "Заполните поле",urlSuffix.fieldName);
                toaster.pop('warning', "Заполните поля");
                return;
            }
            var url =  'http://localhost:8080/document/uploadDocument/' + $ctrl.newDocumentData.documentStudentId + urlSuffix;
            $http.post(url, fd, {
                withCredentials: true,
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined,
                    'Access-Control-Allow-Headers': 'Content-Type',
                    'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(
                function(result){
                    console.log(result)
                    for (var property in $ctrl.newDocumentData) {
                        if ($ctrl.newDocumentData.hasOwnProperty(property)) {
                            delete $ctrl.newDocumentData[property];
                        }
                    }
                    $ctrl.myFile=null;
                    $rootScope.$broadcast('clearFileInput')
                    toaster.pop('success', "Файл загружен в базу данных");
                },
                function(err){
                    console.log(err)
                    toaster.pop('error', "Ошибка загрузки данных на сервер");
                });
        }
    }
})()
