'use strict';
(function(){
    angular.module('dip.directives')
        .directive('blockchainComponent',blockchainComponent)

    function blockchainComponent(){
        return {
            scope: {},
            restrict:"E",
            bindToController: true,
            controller: blockchainComponentCtrl,
            controllerAs: '$ctrl',
            templateUrl: 'components/blockchainComponent/blockchainComponent.html',
        }
    }
    blockchainComponentCtrl.$inject=['$scope','$timeout','$http','$rootScope','$q', 'toaster']
    function blockchainComponentCtrl($scope,$timeout,$http,$rootScope,$q, toaster) {
        var $ctrl = this;
        console.log("blockchainComponentCtrl")
        $ctrl.blockchainTitle = "";
        $ctrl.firstHash = "";
        $ctrl.secondHash = "";
        $ctrl.hashes = [];
        $ctrl.hashesCompareResult=null;
        $ctrl.checkHashes = checkHashes;
        $ctrl.createBlockchain = createBlockchain;
        $ctrl.getHasheById = getHasheById;
        function createBlockchain(){
            if(!$ctrl.blockchainTitle){
                toaster.pop('warning', "Введите Title blockchain");
                return
            }
            var blockchain = {blockchainData:[],date:$ctrl.blockchainDate};
            if($ctrl.previousHash){
                blockchain.previousHash=$ctrl.previousHash
            }
            var o ={};
            o.creationDate = $ctrl.documentDate;
            o.title = $ctrl.blockchainTitle;
            blockchain.blockchainData.push(o)

            //fd.append('blockchain', blockchain);
            console.log(blockchain);
            var url =  'http://localhost:8080/blockchain/create';
            $http.post(url, blockchain).then(
                function(result){
                    //console.log(result)
                    toaster.pop('success', "Blockhain создан");
                },
                function(err){
                    console.log(err)
                    toaster.pop('error', "Ошибка создания Blockchain");
                });
        }
        function checkHashes(){
            if(!$ctrl.hashes){
                toaster.pop('warning', "Введите хэши блокчейнов");
                return
            }
            $http.get('http://localhost:8080/blockchain/checkHashes?' + getHashesForRequest($ctrl.hashes), {

            }).then(function(response){
                $ctrl.hashesCompareResult=response.data.document;
            }).catch(function (err) {
                console.log(err)

            });
        }
        function getHashesForRequest(hashes) {
            if(typeof hashes=='string'){
                var arr = hashes.split(/\r?\n/)
            }else{
                var arr = hashes;
            }

            var s = '';
            arr.forEach(function (item) {
                if(s){
                    s+='&'
                }
                s+='hashes='+item;
            })
            return s;

        }
        function getHasheById(){
            if(!$ctrl.hashId){
                toaster.pop('warning', "Введите id хэши");
                return
            }
            $http.get('http://localhost:8080/blockchain/'+$ctrl.hashId, {

            }).then(function(response){
                $ctrl.hasheInfo=response.data;
            }).catch(function (err) {
                console.log(err)

            });
        }
    }
})();
