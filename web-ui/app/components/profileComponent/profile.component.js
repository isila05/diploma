(function(){
    angular.module('dip.directives')
        .directive('profileComponent',profileComponent)
    function profileComponent(){
        return {
            scope: {},
            restrict:"E",
            bindToController: true,
            controller: profileComponentCtrl,
            controllerAs: '$ctrl',
            templateUrl: 'components/profileComponent/profileComponent.html',
        }
    }
    profileComponentCtrl.$inject=['$scope','$timeout','$http','$rootScope','$q', 'toaster']
    function profileComponentCtrl($scope,$timeout,$http,$rootScope,$q, toaster) {
        var $ctrl = this;
        $ctrl.firstStudentId = "";
        $ctrl.secondStudentId = "";
        $ctrl.isPublic = false;
        $ctrl.getStudentsDocuments = getStudentsDocuments;
        function getStudentsDocuments(){
            if(!$ctrl.firstStudentId || !$ctrl.secondStudentId){
                toaster.pop('warning', "Введите ID студентов");
                return
            }
            if($ctrl.firstStudentId && $ctrl.secondStudentId){
                $http.get('http://localhost:8080/document/documentsByStudents?' + getGetRequest(), {

                }).then(function(response){
                    console.log(response.data)
                    $ctrl.studetData = null;
                    $ctrl.result=response.data;
                }).catch(function (err) {
                    console.log(err)

                });
            }else
            {
                return
            }
        }
        function getGetRequest() {
            var s = 'firstStudentId=' + $ctrl.firstStudentId + '&secondStudentId=' + $ctrl.secondStudentId;
            console.log($ctrl.firstStudentId);
            console.log($ctrl.secondStudentId);
            if($ctrl.isPublic){
                s += '&isPublic=true'
            }else{
                s += '&isPublic=false'
            }
            console.log(s);
            return s;

        }
    }
})()