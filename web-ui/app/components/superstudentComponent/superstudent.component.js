(function(){
    angular.module('dip.directives')
        .directive('superstudentComponent',superstudentComponent)
    function superstudentComponent(){
        return {
            scope: {},
            restrict:"E",
            bindToController: true,
            controller: superstudentComponentCtrl,
            controllerAs: '$ctrl',
            templateUrl: 'components/superstudentComponent/superstudentComponent.html',
        }
    }
    superstudentComponentCtrl.$inject=['$scope','$timeout','$http','$rootScope','$q', 'toaster']
    function superstudentComponentCtrl($scope,$timeout,$http,$rootScope,$q, toaster) {
        var $ctrl = this;
        $ctrl.studentId = "";
        $ctrl.isPublic = false;
        $ctrl.checkSuperStudent = checkSuperStudent;
        function checkSuperStudent(){
            if(!$ctrl.studentId){
                toaster.pop('warning', "Введите ID студента");
                return
            }
            if($ctrl.studentId){
                $http.get('http://localhost:8080/document/checkWithSuperStudent?' + getGetRequest(), {

                }).then(function(response){
                    console.log(response.data)
                    $ctrl.studetData = null;
                    $ctrl.result=response.data;
                }).catch(function (err) {
                    console.log(err)

                });
            }else
            {
                return
            }
        }
        function getGetRequest() {
            var s = 'studentId=' + $ctrl.studentId;
            console.log($ctrl.studentId);
            console.log(s);
            return s;

        }
    }
})()