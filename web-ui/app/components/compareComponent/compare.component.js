'use strict';
(function(){
    angular.module('dip.directives')
        .directive('compareComponent',compareComponent)

    function compareComponent(){
        return {
            scope: {},
            restrict:"E",
            bindToController: true,
            controller: compareComponentCtrl,
            controllerAs: '$ctrl',
            templateUrl: 'components/compareComponent/compareComponent.html',
        }
    }

    compareComponentCtrl.$inject=['$scope','$timeout','$http','$rootScope','$q', 'toaster']
    function compareComponentCtrl($scope,$timeout,$http,$rootScope,$q, toaster) {
        var $ctrl = this;
        $ctrl.myFile1=null;
        $ctrl.myFile2=null;
        $ctrl.myFile3=null;
        $ctrl.title=null;
        $ctrl.title1=null;
        $ctrl.title2=null;
        $ctrl.documents=[];
        $ctrl.compareDocuments=compareDocuments;
        $ctrl.compareDocuments1=compareDocuments1;
        $ctrl.compareDocuments2=compareDocuments2;
        $ctrl.clearResultCompare=clearResultCompare;
        $ctrl.clearResultCompare1=clearResultCompare1;
        $ctrl.clearResultCompare2=clearResultCompare2;
        $ctrl.filterTitle=filterTitle;

        activate()

        function activate() {
            getDocuments()
        }
        function compareDocuments() {
            if(!$ctrl.myFile || !$ctrl.myFile1){
                //toaster.pop('warning', "Заполните поля",'ID студента');
                toaster.pop('warning', "Загрузите документы");
                return;
            }
            var fd = new FormData();
            fd.append('first', $ctrl.myFile);
            fd.append('second', $ctrl.myFile1);

            var url =  'http://localhost:8080/student/getFilesIdentity';
            $http.post(url, fd, {
                withCredentials: true,
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined,
                    'Access-Control-Allow-Headers': 'Content-Type',
                    'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(
                function(result){
                    console.log(result)
                    $ctrl.resultCompare=result.data;

                    $ctrl.myFile=null;
                    $ctrl.myFile1=null;
                    $rootScope.$broadcast('clearFileInput')
                    //toaster.pop('success', "Файл загружен в базу данных");
                },
                function(err){
                    console.log(err)
                    toaster.pop('error', "Ошибка загрузки данных на сервер");
                });
        }
        function clearResultCompare() {
            $ctrl.resultCompare=null;
        }
        function compareDocuments1() {
            if(!$ctrl.myFile2 || !$ctrl.title){
                //toaster.pop('warning', "Заполните поля",'ID студента');
                toaster.pop('warning', "Загрузите документы");
                return;
            }
            var fd = new FormData();
            fd.append('file', $ctrl.myFile2);
            fd.append('title', $ctrl.title.title);

            var url =  'http://localhost:8080/document/getFileIdentity';
            $http.post(url, fd, {
                withCredentials: true,
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined,
                    'Access-Control-Allow-Headers': 'Content-Type',
                    'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(
                function(result){
                    //console.log(result)
                    $ctrl.resultCompare1=result.data;

                    $ctrl.myFile2=null;
                    $ctrl.title=null;
                    $rootScope.$broadcast('clearFileInput')
                    //toaster.pop('success', "Файл загружен в базу данных");
                },
                function(err){
                    console.log(err)
                    toaster.pop('error', "Ошибка загрузки данных на сервер");
                });
        }
        function clearResultCompare1() {
            $ctrl.resultCompare1=null;
        }
        function compareDocuments2(form) {
            console.log(form)
            if(!$ctrl.title1 || !$ctrl.title2){
                //toaster.pop('warning', "Заполните поля",'ID студента');
                toaster.pop('warning', "Загрузите документы");
                return;
            }
            var fd = new FormData();
            fd.append('firstTitle', $ctrl.title1.title);
            fd.append('secondTitle', $ctrl.title2.title);

            var url =  'http://localhost:8080/document/titles/getFileIdentity';
            $http.post(url, fd, {
                withCredentials: true,
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined,
                    'Access-Control-Allow-Headers': 'Content-Type',
                    'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(
                function(result){
                    //console.log(result)
                    $ctrl.resultCompare2=result.data;

                    $ctrl.title1=null;
                    $ctrl.title2=null;
                    $rootScope.$broadcast('clearFileInput')
                    //toaster.pop('success', "Файл загружен в базу данных");
                },
                function(err){
                    console.log(err)
                    toaster.pop('error', "Ошибка загрузки данных на сервер");
                });
        }
        function clearResultCompare2() {
            $ctrl.resultCompare2=null;
        }
        function getDocuments() {
            var url =  'http://localhost:8080/document/documentList';
            $http.get(url).then(function(result){
                var arr=[];
                for(var key in result.data){
                    result.data[key].forEach(function (d) {
                        d.key=key;
                        arr.push(d)
                    })
                }
                $ctrl.documents=arr;
            },
            function(err){
                console.log(err)
                toaster.pop('error', "Ошибка получения документов");
            });
        }
        function filterTitle(item){
            return item.key
        }
    }
})()
