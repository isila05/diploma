angular.module('dip', ['ngRoute',
    'ui.router',
    'ui.router.state.events',
    'ngResource',
    'dip.controllers',
    'dip.services',
    'dip.directives',
    'dip.filters',
    //'dip.exception',
    'ui.bootstrap',
    'ngTouch',
    'ngAnimate',
    'ui.select',
    'satellizer',
    'ngMessages',
    'toaster'
])

.run(['$rootScope', '$state', '$stateParams',function ($rootScope,$state,$stateParams){
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    $rootScope.$on('$stateChangeStart', function(event, to, toParams, fromState, fromParams){

    });
    $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {

    });
    $rootScope.$on('$stateChangeError', function(event,toState, toParams, fromState, fromParams, error) {
           console.log(error);});



}])
.config(['$stateProvider', '$urlRouterProvider','$locationProvider',function ($stateProvider,$urlRouterProvider,$locationProvider){
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');
    $urlRouterProvider
        .when('/home','/')
        .otherwise('/404');
    $stateProvider
        .state("404", {
            url: "/404",
            templateUrl:'views/template/partials/404.html',
            controller: '404Ctrl'
        })
        .state("home", {
            url: "/",
            template:'<home-component></home-component>'
        })


        .state('compare', {
            url: '/compare',
            template:'<compare-component></compare-component>'
        })
        .state('profile', {
            url: '/profile',
            template:'<profile-component></profile-component>'
        })
        .state('blockchain', {
            url: '/blockchain',
            template:'<blockchain-component></blockchain-component>'
        })
        .state('superstudent', {
            url: '/superstudent',
            template:'<superstudent-component></superstudent-component>'
        })
}]);







