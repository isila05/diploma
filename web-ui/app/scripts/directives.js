'use strict';

/* Directives */

angular.module('dip.directives', [])
    .directive('fileModel', ['$parse','$timeout', function ($parse,$timeout) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;
                element.bind("change", function (changeEvent) {
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            modelSetter(scope, changeEvent.target.files[0]);
                        });
                    }
                    reader.readAsDataURL(changeEvent.target.files[0]);
                });
                scope.$on('clearFileInput',function () {
                    //console.log('clearFileInput');
                    element.val(null);
                })
            }
        };
    }]);






